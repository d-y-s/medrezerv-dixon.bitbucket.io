"use strict";

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

/* ^^^
 * Глобальные-вспомогательные функции
 * ========================================================================== */

/**
  * Возвращает HTML-код иконки из SVG-спрайта
  *
  * @param {String} name Название иконки из спрайта
  * @param {Object} opts Объект настроек для SVG-иконки
  *
  * @example SVG-иконка
  * getSVGSpriteIcon('some-icon', {
  *   tag: 'div',
  *   type: 'icons', // colored для подключения иконки из цветного спрайта
  *   class: '', // дополнительные классы для иконки
  *   mode: 'inline', // external для подключаемых спрайтов
  *   url: '', // путь до файла спрайта, необходим только для подключаемых спрайтов
  * });
  */
function getSVGSpriteIcon(name, opts) {
  opts = _extends({
    tag: 'div',
    type: 'icons',
    "class": '',
    mode: 'inline',
    url: ''
  }, opts);
  var external = '';
  var typeClass = '';

  if (opts.mode === 'external') {
    external = "".concat(opts.url, "/sprite.").concat(opts.type, ".svg");
  }

  if (opts.type !== 'icons') {
    typeClass = " svg-icon--".concat(opts.type);
  }

  opts["class"] = opts["class"] ? " ".concat(opts["class"]) : '';
  return "\n        <".concat(opts.tag, " class=\"svg-icon svg-icon--").concat(name).concat(typeClass).concat(opts["class"], "\" aria-hidden=\"true\" focusable=\"false\">\n        <svg class=\"svg-icon__link\">\n            <use xlink:href=\"").concat(external, "#").concat(name, "\"></use>\n        </svg>\n        </").concat(opts.tag, ">\n    ");
}
/* ^^^
 * JQUERY Actions
 * ========================================================================== */


$(function () {
  'use strict';
  /**
   * определение существования элемента на странице
   */

  $.exists = function (selector) {
    return $(selector).length > 0;
  };

  $('.remodal__inner .myForm').on('submit', function (event) {
    event.preventDefault();
    $('.remodal__notification').slideDown();
    $('.remodal__inner .myForm').slideUp();
  });
  $.each($('.l-catalog__menu ul').find('> li'), function (index, element) {
    if ($(element).find(' > ul').length) {
      var triggerIcon = ['<div class="svg-icon svg-icon--angle-down">', '</div>'].join('');
      var subMenuTrigger = $('<div class="sub-menu-trigger">' + triggerIcon + '</div>');
      $(element).addClass('haschild').append(subMenuTrigger);
    }
  });
  var mobileMenu = $('.l-catalog').clone();
  $(mobileMenu).addClass('catalogMenu');
  $('.m-catalogMenu').append(mobileMenu);

  if ($('.l-catalog').hasClass('catalogMenu')) {
    $('.sub-menu-trigger').on('click', function (event) {
      $(this).toggleClass('rotade');

      if (!$(this).closest('li').find('>ul').length) {
        return;
      }

      event.preventDefault();
      $(this).closest('li').toggleClass('open').find('>ul').stop().slideToggle();
    });
  }

  $('.js-main-slider').owlCarousel({
    items: 1,
    loop: true,
    nav: true,
    dots: false,
    autoplay: true,
    autoplayTimeout: 5000,
    autoplayHoverPause: true,
    mouseDrag: false,
    animateOut: 'fadeOut',
    animateIn: 'slideOutin',
    navText: ['<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 477.175 477.175" style="enable-background:new 0 0 477.175 477.175;" xml:space="preserve"><g><path d="M360.731,229.075l-225.1-225.1c-5.3-5.3-13.8-5.3-19.1,0s-5.3,13.8,0,19.1l215.5,215.5l-215.5,215.5c-5.3,5.3-5.3,13.8,0,19.1c2.6,2.6,6.1,4,9.5,4c3.4,0,6.9-1.3,9.5-4l225.1-225.1C365.931,242.875,365.931,234.275,360.731,229.075z"/></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></svg>', '<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 477.175 477.175" style="enable-background:new 0 0 477.175 477.175;" xml:space="preserve"><g><path d="M360.731,229.075l-225.1-225.1c-5.3-5.3-13.8-5.3-19.1,0s-5.3,13.8,0,19.1l215.5,215.5l-215.5,215.5c-5.3,5.3-5.3,13.8,0,19.1c2.6,2.6,6.1,4,9.5,4c3.4,0,6.9-1.3,9.5-4l225.1-225.1C365.931,242.875,365.931,234.275,360.731,229.075z"/></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></svg>'],
    onInitialized: counter,
    onTranslated: counter
  });

  function counter(event) {
    if (!event.namespace) {
      return;
    }

    var slides = event.relatedTarget;
    $('.main-slider__counter').html(slides.relative(slides.current()) + 1 + '<span>' + ' /' + slides.items().length + '</span>');
  }

  $('.js-btn-burger').on('click', function (e) {
    e.preventDefault();
    $(this).toggleClass('btn-burger-is-active');
    $('.mobile-panel .menu').toggleClass('menu-is-active');
    $('.mobile-panel__overlay-menu').toggleClass('overlay-is-active');
  });
  $('.js-btn-search').on('click', function (e) {
    e.preventDefault();
    $('.search').toggleClass('is-active-search').stop().slideToggle();
  });
  $.each($('.menu__nav-list').find('> li'), function (index, element) {
    if ($(element).find(' > ul').length) {
      var triggerIcon = ['<div class="svg-icon svg-icon--angle-down">', '</div>'].join('');
      var subMenuTrigger = $('<div class="sub-menu-trigger">' + triggerIcon + '</div>');
      $(element).addClass('haschild').append(subMenuTrigger);
    }
  });
  $('.menu__nav-list .sub-menu-trigger').on('click', function (event) {
    $(this).toggleClass('rotade');

    if (!$(this).closest('li').find('>ul').length) {
      return;
    }

    event.preventDefault();
    $(this).closest('li').toggleClass('open').find('>ul').stop().slideToggle();
  });
  $('.our-offers-products__tabs-btn-items').on('click', '.js-btn-item', function () {
    $(this).addClass('is-active-tab-btn').siblings().removeClass('is-active-tab-btn');
    $('.our-offers-products__inner').find('.js-tab-content').removeClass('is-active-tab-content').hide().eq($(this).index()).fadeIn();
  });
  $('.our-offers-products__content--new-offer .js-slider-our-offers, .our-offers-products__content--special-offer .js-slider-our-offers, .our-offers-products__content--coming-offer .js-slider-our-offers').owlCarousel({
    items: 4,
    loop: true,
    nav: true,
    dots: true,
    autoplay: true,
    autoplayTimeout: 5000,
    autoplayHoverPause: true,
    mouseDrag: true,
    navText: ['<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 477.175 477.175" style="enable-background:new 0 0 477.175 477.175;" xml:space="preserve"><g><path d="M360.731,229.075l-225.1-225.1c-5.3-5.3-13.8-5.3-19.1,0s-5.3,13.8,0,19.1l215.5,215.5l-215.5,215.5c-5.3,5.3-5.3,13.8,0,19.1c2.6,2.6,6.1,4,9.5,4c3.4,0,6.9-1.3,9.5-4l225.1-225.1C365.931,242.875,365.931,234.275,360.731,229.075z"/></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></svg>', '<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 477.175 477.175" style="enable-background:new 0 0 477.175 477.175;" xml:space="preserve"><g><path d="M360.731,229.075l-225.1-225.1c-5.3-5.3-13.8-5.3-19.1,0s-5.3,13.8,0,19.1l215.5,215.5l-215.5,215.5c-5.3,5.3-5.3,13.8,0,19.1c2.6,2.6,6.1,4,9.5,4c3.4,0,6.9-1.3,9.5-4l225.1-225.1C365.931,242.875,365.931,234.275,360.731,229.075z"/></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></svg>'],
    responsive: {
      320: {
        items: 1,
        dots: false
      },
      640: {
        items: 2,
        dots: false
      },
      960: {
        items: 3
      },
      1280: {
        items: 4
      }
    }
  });
  var PAGE = $('html, body');
  var pageScroller = $('.page-scroller');
  var inMemoryClass = 'page-scroller--memorized';
  var isVisibleClass = 'page-scroller--visible';
  var enabledOffset = 60;
  var pageYOffset = 0;
  var inMemory = false;

  function resetPageScroller() {
    setTimeout(function () {
      if (window.pageYOffset > enabledOffset) {
        pageScroller.addClass(isVisibleClass);
      } else if (!pageScroller.hasClass(inMemoryClass)) {
        pageScroller.removeClass(isVisibleClass);
      }
    }, 150);

    if (!inMemory) {
      pageYOffset = 0;
      pageScroller.removeClass(inMemoryClass);
    }

    inMemory = false;
  }

  if (pageScroller.length > 0) {
    window.addEventListener('scroll', resetPageScroller, window.supportsPassive ? {
      passive: true
    } : false);
    pageScroller.on('click', function (event) {
      event.preventDefault();
      window.removeEventListener('scroll', resetPageScroller);

      if (window.pageYOffset > 0 && pageYOffset === 0) {
        inMemory = true;
        pageYOffset = window.pageYOffset;
        pageScroller.addClass(inMemoryClass);
        PAGE.stop().animate({
          scrollTop: 0
        }, 500, 'swing', function () {
          window.addEventListener('scroll', resetPageScroller, window.supportsPassive ? {
            passive: true
          } : false);
        });
      } else {
        pageScroller.removeClass(inMemoryClass);
        PAGE.stop().animate({
          scrollTop: pageYOffset
        }, 500, 'swing', function () {
          pageYOffset = 0;
          window.addEventListener('scroll', resetPageScroller, window.supportsPassive ? {
            passive: true
          } : false);
        });
      }
    });
  }

  $('.js-slider-reviews').owlCarousel({
    items: 1,
    loop: true,
    nav: true,
    dots: true,
    autoplay: true,
    autoplayTimeout: 5000,
    autoplayHoverPause: true,
    mouseDrag: true,
    navText: ['<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 477.175 477.175" style="enable-background:new 0 0 477.175 477.175;" xml:space="preserve"><g><path d="M360.731,229.075l-225.1-225.1c-5.3-5.3-13.8-5.3-19.1,0s-5.3,13.8,0,19.1l215.5,215.5l-215.5,215.5c-5.3,5.3-5.3,13.8,0,19.1c2.6,2.6,6.1,4,9.5,4c3.4,0,6.9-1.3,9.5-4l225.1-225.1C365.931,242.875,365.931,234.275,360.731,229.075z"/></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></svg>', '<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 477.175 477.175" style="enable-background:new 0 0 477.175 477.175;" xml:space="preserve"><g><path d="M360.731,229.075l-225.1-225.1c-5.3-5.3-13.8-5.3-19.1,0s-5.3,13.8,0,19.1l215.5,215.5l-215.5,215.5c-5.3,5.3-5.3,13.8,0,19.1c2.6,2.6,6.1,4,9.5,4c3.4,0,6.9-1.3,9.5-4l225.1-225.1C365.931,242.875,365.931,234.275,360.731,229.075z"/></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></svg>'],
    responsive: {
      320: {
        dots: false
      },
      1280: {
        dots: true
      }
    }
  });
  $('.js-silder-view-products').owlCarousel({
    items: 3,
    loop: true,
    nav: true,
    dots: true,
    autoplay: true,
    autoplayTimeout: 5000,
    autoplayHoverPause: true,
    mouseDrag: true,
    navText: ['<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 477.175 477.175" style="enable-background:new 0 0 477.175 477.175;" xml:space="preserve"><g><path d="M360.731,229.075l-225.1-225.1c-5.3-5.3-13.8-5.3-19.1,0s-5.3,13.8,0,19.1l215.5,215.5l-215.5,215.5c-5.3,5.3-5.3,13.8,0,19.1c2.6,2.6,6.1,4,9.5,4c3.4,0,6.9-1.3,9.5-4l225.1-225.1C365.931,242.875,365.931,234.275,360.731,229.075z"/></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></svg>', '<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 477.175 477.175" style="enable-background:new 0 0 477.175 477.175;" xml:space="preserve"><g><path d="M360.731,229.075l-225.1-225.1c-5.3-5.3-13.8-5.3-19.1,0s-5.3,13.8,0,19.1l215.5,215.5l-215.5,215.5c-5.3,5.3-5.3,13.8,0,19.1c2.6,2.6,6.1,4,9.5,4c3.4,0,6.9-1.3,9.5-4l225.1-225.1C365.931,242.875,365.931,234.275,360.731,229.075z"/></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></svg>'],
    responsive: {
      320: {
        items: 1,
        dots: false
      },
      640: {
        items: 2,
        dots: false
      },
      960: {
        items: 3,
        dots: false
      },
      1025: {
        dots: true
      }
    }
  });
  var slider = $(".js-cart-product-slider");
  var thumbnailSlider = $(".js-thumb-container");
  var duration = 500;
  var syncedSecondary = true;
  setTimeout(function () {
    $(".cloned .item-slider-model a").attr("data-fancybox", "group-2");
  }, 500); // carousel function for main slider

  slider.owlCarousel({
    items: 1,
    loop: true,
    nav: true,
    dots: false,
    autoplay: true,
    thumbs: true,
    autoplayTimeout: 5000,
    autoplayHoverPause: true,
    mouseDrag: false,
    animateOut: 'fadeOut',
    animateIn: 'slideOutin',
    navText: ['<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 477.175 477.175" style="enable-background:new 0 0 477.175 477.175;" xml:space="preserve"><g><path d="M360.731,229.075l-225.1-225.1c-5.3-5.3-13.8-5.3-19.1,0s-5.3,13.8,0,19.1l215.5,215.5l-215.5,215.5c-5.3,5.3-5.3,13.8,0,19.1c2.6,2.6,6.1,4,9.5,4c3.4,0,6.9-1.3,9.5-4l225.1-225.1C365.931,242.875,365.931,234.275,360.731,229.075z"/></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></svg>', '<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 477.175 477.175" style="enable-background:new 0 0 477.175 477.175;" xml:space="preserve"><g><path d="M360.731,229.075l-225.1-225.1c-5.3-5.3-13.8-5.3-19.1,0s-5.3,13.8,0,19.1l215.5,215.5l-215.5,215.5c-5.3,5.3-5.3,13.8,0,19.1c2.6,2.6,6.1,4,9.5,4c3.4,0,6.9-1.3,9.5-4l225.1-225.1C365.931,242.875,365.931,234.275,360.731,229.075z"/></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></svg>']
  }).on("changed.owl.carousel", syncPosition); // carousel function for thumbnail slider

  thumbnailSlider.on("initialized.owl.carousel", function () {
    thumbnailSlider.find(".owl-item").eq(0).addClass("current");
  }).owlCarousel({
    items: 3,
    loop: false,
    nav: false,
    mouseDrag: false,
    smartSpeed: 600
  }).on("changed.owl.carousel", syncPosition2); // on click thumbnaisl

  thumbnailSlider.on("click", ".owl-item", function (e) {
    e.preventDefault();
    var number = $(this).index();
    slider.data("owl.carousel").to(number, 300, true);
  });

  function syncPosition(el) {
    var count = el.item.count - 1;
    var current = Math.round(el.item.index - el.item.count / 2 - 0.5);

    if (current < 0) {
      current = count;
    }

    if (current > count) {
      current = 0;
    }

    thumbnailSlider.find(".owl-item").removeClass("current").eq(current).addClass("current");
    var onscreen = thumbnailSlider.find(".owl-item.active").length - 1;
    var start = thumbnailSlider.find(".owl-item.active").first().index();
    var end = thumbnailSlider.find(".owl-item.active").last().index();

    if (current > end) {
      thumbnailSlider.data("owl.carousel").to(current, 100, true);
    }

    if (current < start) {
      thumbnailSlider.data("owl.carousel").to(current - onscreen, 100, true);
    }
  }

  function syncPosition2(el) {
    if (syncedSecondary) {
      var number = el.item.index;
      slider.data("owl.carousel").to(number, 100, true);
    }
  }

  $('.js-3D-viewing-spin').spritespin({
    source: ['img/v-3d-1.jpg', 'img/v-3d-2.jpg', 'img/v-3d-3.jpg', 'img/v-3d-4.jpg'],
    width: 420,
    height: 420,
    frameTime: 360,
    // responsive: true,
    sense: -1
  });
  $('.js-privacy-policy-custom-scroll').scrollbox();
}); // contacts map

var center1 = {
  lat: 55.806886,
  lng: 37.56848773578479
};

function initStandortMap() {
  var standortMap = document.getElementById('standort-map');

  if (!standortMap) {
    return;
  }

  var map = new google.maps.Map(standortMap, {
    center: {
      lat: center1.lat,
      lng: center1.lng
    },
    disableDefaultUI: true,
    zoom: 16
  });
  var marker2 = new google.maps.Marker({
    position: {
      lat: center1.lat,
      lng: center1.lng
    },
    map: map,
    icon: 'img/marker.png'
  });
  var infowindow = new google.maps.InfoWindow({
    pixelOffset: new google.maps.Size(0, 155)
  }); // Section map

  var objectPoint = new google.maps.LatLng(center1.lat, center1.lng),
      infowindow,
      service,
      markers = [];

  function createMarker(place, iconType) {
    // var placeLoc = place.geometry.location;
    var marker = new google.maps.Marker({
      map: map,
      icon: icon,
      position: place.geometry.location
    });
    marker.myType = myType;
    markers.push(marker);
  }
}

function initMap() {
  initStandortMap();
}