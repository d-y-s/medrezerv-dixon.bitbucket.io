$.each($('.l-catalog__menu ul').find('> li'), function(index, element) {
    
    if ($(element).find(' > ul').length) {

        var triggerIcon = ['<div class="svg-icon svg-icon--angle-down">','</div>'].join('');

        var subMenuTrigger = $('<div class="sub-menu-trigger">' + triggerIcon + '</div>');

        $(element)
            .addClass('haschild')
            .append(subMenuTrigger);
    }
});

var mobileMenu = $('.l-catalog').clone();
$(mobileMenu).addClass('catalogMenu');
$('.m-catalogMenu').append(mobileMenu);


if ($('.l-catalog').hasClass('catalogMenu')) {
	$('.sub-menu-trigger').on('click', function(event) {
	    $(this).toggleClass('rotade');
	    if(!$(this).closest('li').find('>ul').length){
	        return;
	    }
	    event.preventDefault();
	    $(this).closest('li').toggleClass('open').find('>ul').stop().slideToggle();
	});	
}